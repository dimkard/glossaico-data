import json
import collections
import re
from pathlib import Path
import shutil
import zipfile
import requests
from requests.adapters import HTTPAdapter
from urllib3.util import Retry
from librelingo_yaml_loader import load_course
from librelingo_json_export.export import export_course


def main():
    lili_url = "https://github.com/LibreLingo/LibreLingo/archive/main.zip"
    lili_file = "./lili.zip"
    courses_config_file = "../courses.json"
    images_dir = "../images"
    voices_dir = "../voice"

    download(lili_url, lili_file)
    extract_courses_cfg(lili_file, courses_config_file)
    deploy_courses(courses_config_file)
    deploy_images(lili_file, images_dir)
    deploy_voice(lili_file, voices_dir)
    Path(f"{lili_file}").unlink()


def extract_courses_cfg(lili_file, courses_config_file):
    # Get the courses JSON config file

    with zipfile.ZipFile(lili_file, "r") as zip_obj:
        for member_name in zip_obj.namelist():
            pattern = "^.*config/courses.json"
            if re.match(pattern, member_name):
                with zip_obj.open(member_name) as file_in_zip, open(
                    courses_config_file, "wb"
                ) as target_file:
                    shutil.copyfileobj(file_in_zip, target_file)


def deploy_courses(courses_config_file):
    # Deploy courses
    with open(courses_config_file, "r", encoding="utf-8") as courses_file:
        courses_json = json.load(courses_file)
        for course in courses_json:
            if course["deploy"]:
                print("Deploy", course["paths"]["jsonFolder"])
                update_course_cfg(
                    course["url"],
                    course["paths"]["jsonFolder"],
                    course["paths"]["yamlFolder"],
                )
                export_course_data(course["paths"]["jsonFolder"])


def deploy_images(lili_file, images_dir):
    print("Deploy images")
    # Clear previous images
    images_path = Path(images_dir)
    if images_path.is_dir():
        shutil.rmtree(images_path)
    images_path.mkdir(parents=True, exist_ok=True)

    # Deploy images
    with zipfile.ZipFile(lili_file, "r") as zip_obj:
        for member_name in zip_obj.namelist():
            pattern = r"^.*/apps/web/static/images/.*_tiny\.jpg"
            if re.match(pattern, member_name):
                print(member_name)
                image_name_match = re.search(r"\/([^\/]+.jpg)", member_name)
                if image_name_match:
                    with zip_obj.open(member_name) as file_in_zip, open(
                        f"{images_dir}/{image_name_match.group(1)}", "wb"
                    ) as target_file:
                        shutil.copyfileobj(file_in_zip, target_file)


def deploy_voice(lili_file, voices_dir):
    print("Deploy voice")
    # Clear previous voices
    voices_path = Path(voices_dir)
    if voices_path.is_dir():
        shutil.rmtree(voices_path)
    voices_path.mkdir(parents=True, exist_ok=True)

    # Deploy voices
    with zipfile.ZipFile(lili_file, "r") as zip_obj:
        for member_name in zip_obj.namelist():
            pattern = r"^.*/apps/web/static/voice/.*\.mp3"
            if re.match(pattern, member_name):
                print(member_name)
                voice_name_match = re.search(r"\/([^\/]+.mp3)", member_name)
                if voice_name_match:
                    with zip_obj.open(member_name) as file_in_zip, open(
                        f"{voices_dir}/{voice_name_match.group(1)}", "wb"
                    ) as target_file:
                        shutil.copyfileobj(file_in_zip, target_file)


def download(remote_url, local_path):
    retry_on_status = [429, 500, 502, 503, 504, 104]
    retry_strategy = Retry(total=3, status_forcelist=retry_on_status)

    adapter = HTTPAdapter(max_retries=retry_strategy)

    session = requests.Session()
    session.mount("http://", adapter)
    session.mount("https://", adapter)

    print("Download", remote_url, "to", local_path)
    response = session.get(remote_url)
    if response.status_code != 200:
        print("Cannot download from", remote_url)
        raise requests.HTTPError(
            f"Cannot download from {remote_url}, {response.status_code}, {response.reason}"
        )

    with open(local_path, "wb") as local_file:
        local_file.write(response.content)


def update_course_cfg(course_url, json_folder, yaml_folder):
    print("Update course yaml")
    # Update course configuration (yaml data)
    course_file = Path(f"{json_folder}.zip")

    # Download course archive
    download(course_url, course_file)

    zip_root_dir = ""
    with zipfile.ZipFile(course_file, "r") as zip_obj:
        for info_obj in zip_obj.infolist():
            if info_obj.is_dir() and re.match(r"^[^/]*/$", info_obj.filename):
                zip_root_dir = info_obj.filename

    if not zip_root_dir:
        raise RuntimeError("Root directory of the zip folder cannot be found")

    # Temporarily extract the course archive
    if Path(yaml_folder).is_dir():
        shutil.rmtree(zip_root_dir)

    with zipfile.ZipFile(course_file, "r") as zip_obj:
        for member_name in zip_obj.namelist():
            if member_name.startswith(zip_root_dir):
                zip_obj.extract(member_name)

    # Delete old course content
    yaml_course_path = Path(f"../courses/yaml/{json_folder}/")
    if Path(yaml_course_path).is_dir():
        shutil.rmtree(yaml_course_path)

    # Move content
    yaml_course_path.mkdir(parents=True, exist_ok=True)
    shutil.move(f"{zip_root_dir}/course/", yaml_course_path)

    # Clear temporary content
    shutil.rmtree(zip_root_dir)
    course_file.unlink()


def export_course_data(json_folder):
    print("Export course")
    # Delete old exported content
    yaml_export_path = Path(f"../courses/export/{json_folder}/")
    if Path(yaml_export_path).is_dir():
        shutil.rmtree(yaml_export_path)

    # Export content
    yaml_export_path.mkdir(parents=True, exist_ok=True)

    Settings = collections.namedtuple(
        "Settings",
        [
            "dry_run",
        ],
    )

    settings = Settings(dry_run=False)

    course = load_course(f"../courses/yaml/{json_folder}/course")
    export_course(yaml_export_path, course, settings)


if __name__ == "__main__":
    main()
