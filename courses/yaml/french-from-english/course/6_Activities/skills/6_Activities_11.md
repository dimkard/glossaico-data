# 🇫🇷 Apprendre des langues 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**In this lesson we will learn vocabulary to talk about languages**  
*Dans cette leçon, nous apprendrons le vocabulaire pour parler de l'apprentissage des langues*.  

## Vocabulary 
🇺🇸 french          | 🇫🇷 français   

🇺🇸 german          | 🇫🇷 allemand  

🇺🇸 english         | 🇫🇷 anglais  

🇺🇸 vocabulary list | 🇫🇷 vocabulary list  


