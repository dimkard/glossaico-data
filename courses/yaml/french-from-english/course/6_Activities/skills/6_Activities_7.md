# 🇫🇷 Culturel 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**In this lesson we will learn vocabulary to talk about culture**  
*Dans cette leçon, nous apprendrons le vocabulaire pour parler de la culture* 

## Vocabulary 
🇺🇸 theater        | 🇫🇷 théâtre  

🇺🇸 chess          | 🇫🇷 échecs  

🇺🇸 dinosaures     | 🇫🇷 dinosaures  


