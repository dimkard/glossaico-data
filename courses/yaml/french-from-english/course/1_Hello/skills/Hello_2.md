# 🇫🇷 Hello et Salut 🇫🇷, 

**I bet you're doing great. Let's practice what we've learned.**
Je parie que vous vous débrouillez très bien. Mettons en pratique ce que nous avons appris.

## Vocabulary 
🇺🇸 I am |  🇫🇷 Je suis

🇺🇸 My name is | 🇫🇷 Je m’appelle ...

🇺🇸 What’s your name? (informal) | 🇫🇷 Comment t’appelles-tu ?

🇺🇸  I live in ... | 🇫🇷 J’habite à ...

🇺🇸  I am ... years old | 🇫🇷 J’ai ...  ans.

🇺🇸 Hello | 🇫🇷 Salut

🇺🇸 Yes | 🇫🇷 Oui

🇺🇸 No | 🇫🇷 Non


