# 🇫🇷 Numéros I - 1 - 10 🇫🇷, 

**Nice that you'd like to learn some french.**      

*C'est bien que vous souhaitiez apprendre le français.*      

**We learn in this lesson to count from one to ten.**    

*Nous apprenons dans cette leçon à compter de un à dix.*  

## Vocabulary 
🇺🇸 One |  🇫🇷 Un  
  
🇺🇸 Two | 🇫🇷 Deux  
  
🇺🇸 Three | 🇫🇷 Trois  
  
🇺🇸 Four | 🇫🇷 Quatre  
  
🇺🇸 Five | 🇫🇷 Cinq  
  
🇺🇸 Six | 🇫🇷 Six  
   
🇺🇸 Seven | 🇫🇷 Sept  
  
🇺🇸 Eight | 🇫🇷 Huit  
  
🇺🇸 Nine | 🇫🇷 Neuf  
  
🇺🇸 Ten | 🇫🇷 Dix  

 
