# La Benvenguda sobre LibreLingo !

Bienvenue sur LibreLingo, la plateforme d'apprentissage de langues libre et ouverte !

Dans ce cours d'introduction, vous apprendrez des phrases de base de la vie de tout les jours en
Occitan auvergnat.

### Se saluer

*   __Bonjorn !__ : Bonjour !
*   __Adieu !__: Salut ! (pour dire bonjour aussi bien qu'aurevoir)
*   __A totara !__: A toute à l'heure ! (ou encore, à plus !)

### Se présenter

*   __Me sònan Joan.__: Je m'apelle Jean (mais littéralement, "Elles/Ils m'apellent Jean")
*   __Me 'pèlan Cecilia.__: Autre forme que le verbe "sonar" tout aussi valide. L'apostrophe indique qu'il y avait un "a" (m'apèlan) qui est tombé.
*   __Coma te sònan ?__: Comment tu t'apelles ? (mais littéralement, "Comment elles/ils t'apellent ?")

### Autres phrases

*   __De que se passa ?__: Qu'est ce qu'il se passe ?
*   __Bon 'près-miejorn !__: Bon après-midi ! (pareil, l'apostrophe indique un "a" initial qui est tombé. On apelle cela l'__aphérèse__)
*   __Bona nuèit !__: Bonne nuit !

### Point de Grammaire

Il est important de noter qu'il n'y a en occitan, comme dans la plupart des langues romanes et contrairement aux langues d'oïl, __pas de pronom personnel systématique__ (à l'exception des dialectes auvergnats de Thiers et au dessus). Ils n'apparaissent que pour accentuer la phrase, comme dans les exemples ci dessous:

*   __Sèi__ Joan: __Je suis__ Jean
*   __Iu__, sèi Cecilia: __Moi__, je suis Cécile