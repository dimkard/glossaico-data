# Animaux

Un peu de vocabulaire simple d'occitan auvergnat sur le thème des animaux, pour commencer...

### Quelques articles en occitan auvergnat

*   __un__: un
*   __una__: une
*   __le__ (peut être __lo__ dans certains endroits): le
*   __la__: la