# Bases de l'accord en genre

Comme pour toutes les langues romanes, il existe l'accord en genre (féminin, masculin ou neutre) en occitan auvergnat. Ce cours permettra de maîtriser les cas les plus courants et les plus simples.

Nous verrons ici le passage du masculin au féminin, car il est plus simple de se repérer de cette manière (étant donné que la plupart des mots au féminin se terminent par un "a")

### Le mot masculin se termine par une consonne

Si un mot au masculin se termine par une consonne, il suffit tout simplement d'ajouter un "a" pour le mettre au féminin.

*   cha__t__ -&gt; chat__a__ : chat -&gt; chatte
*   chi__n__ -&gt; chin__a__ : chien -&gt; chienne

### Le mot masculin se termine par un "e"

Si un mot au masculin se termine par la voyelle "e", il suffit de remplacer ce "e" par un "a" pour le mettre au féminin.

*   ors__e__ -&gt; ors__a__ : ours -&gt; ourse
*   gent__e__ -&gt; gent__a__ : joli -&gt; jolie

### Les mots féminins et masculins sont différents

Il arrive évidemment que les mots au féminin et au masculins soient totalement différents (ou partagent malgré tout une racine commune mais restent pas mal différents). C'est le cas nottament et surtout des animaux de la ferme.

*   __vacha__ -&gt; __buòu__ : vache -&gt; boeuf
*   __canard__ -&gt; __cana__ : canard -&gt; cane

### IMPORTANT

Certains mots existent en français sous un certain genre, mais en occitan sous un autre ! Par exemple on ne dit pas couramment __un chavau__ (un cheval) mais bien __una ega__ (une jument). De même qu'on ne dit pas __la pòma__, mais bien __le pòm__ (la pomme). __Vérifiez toujours l'emploi du genre en occitan !__

### Point de Grammaire

Comme dans toute les langues romanes, __on accorde l'adjectif en genre et en nombre__:
- La gent__a__ chin__a__: La jolie chienne

### ATTENTION !

Ce qui est décrit ci dessus sont les règles basiques de l'accord en genre. D'autres sont à venir ! (mais contentez vous de celles là pour commencer)