# 🇫🇷 Faire du shopping 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__In this lesson we will learn vocabulary to talk about culture__
_Dans cette leçon, nous apprendrons le vocabulaire pour parler de la culture_

## Vocabulary

🇺🇸 credit card    | 🇫🇷 carte de crédit  

🇺🇸 chess          | 🇫🇷 échecs  

🇺🇸 dinosaures     | 🇫🇷 dinosaures  