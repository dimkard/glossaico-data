# 🇫🇷 Culturel 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__In this lesson we will learn vocabulary to talk about culture__
_Dans cette leçon, nous apprendrons le vocabulaire pour parler de la culture_

## Vocabulary

🇺🇸 theater        | 🇫🇷 théâtre  

🇺🇸 chess          | 🇫🇷 échecs  

🇺🇸 dinosaures     | 🇫🇷 dinosaures  