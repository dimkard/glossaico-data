# 🇫🇷 Sports nautiques 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__In this lesson we will learn vocabulary to talk about water sport__
_Dans cette leçon, nous apprendrons le vocabulaire pour parler des sports nautiques_

## Vocabulary

🇺🇸 credit card        | 🇫🇷 carte de crédit  

🇺🇸 surfing            | 🇫🇷 surfer  