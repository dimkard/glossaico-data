# 🇫🇷 Radonnée 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__In this lesson we will learn hiking vocabulary__
_Dans cette leçon, nous allons apprendre le vocabulaire de la randonnée_

## Vocabulary

🇺🇸 Hiking         | 🇫🇷 Radonnée  

🇺🇸 the sign       | 🇫🇷 la pancarte  

🇺🇸 the mountain   | 🇫🇷 la montagne  

🇺🇸 bench          | 🇫🇷 banc  

🇺🇸 high           | 🇫🇷 haute  

🇺🇸 une carte      | 🇫🇷 a map  