# 🇫🇷 Weather II 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__Wir lernen in dieser Lektion etwas über das Wetter.__
_Nous allons apprendre quelque chose sur le temps dans cette leçon._

## Vocabulary

🇺🇸 the moon | 🇫🇷 la lune  

🇺🇸 hail     | 🇫🇷 grêle

🇺🇸 tornado  | 🇫🇷 tornade

🇺🇸 temps    | 🇫🇷 weather