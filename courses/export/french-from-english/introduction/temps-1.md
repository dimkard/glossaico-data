# 🇫🇷 Weather 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__Wir lernen in dieser Lektion etwas über das Wetter.__
_Nous allons apprendre quelque chose sur le temps dans cette leçon._

## Vocabulary

🇺🇸 Clouds | 🇫🇷 Nuages  

🇺🇸 Sky    | 🇫🇷 Ciel  

🇺🇸 Wind   | 🇫🇷 Vent  

🇺🇸 Sun    | 🇫🇷 Soleil  

🇺🇸 Moon   | 🇫🇷 Lune  

🇺🇸 Fog    | 🇫🇷 Brouillard  

🇺🇸 Rain   | 🇫🇷 Pluie  

🇺🇸 Hot    | 🇫🇷 chaud  

🇺🇸 Cold   | 🇫🇷 froid  