# 🇫🇷 Hello et Salut 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__We will start with an easy introduction.__
_Nous commencerons par une introduction facile_.   

__To get you used to the french language,__
_Pour vous habituer à la langue française,_
__each sentence will be translated right below into french.__
_chaque phrase sera traduite ci-dessous en français._
__Don't worry, if you do not understand everything right away.__
_Ne vous inquiétez pas si vous ne comprenez pas tout tout de suite._
__Here is what you'll learn in this lesson:__
_ Introducing yourself _ Greet others  

## Vocabulary

🇺🇸 I am |  🇫🇷 Je suis

🇺🇸 My name is | 🇫🇷 Je m’appelle ...

🇺🇸 What’s your name? (informal) | 🇫🇷 Comment t’appelles-tu ?

🇺🇸  I live in ... | 🇫🇷 J’habite à ...

🇺🇸  I am ... years old | 🇫🇷 J’ai ...  ans.

🇺🇸 Hello | 🇫🇷 Salut

🇺🇸 Yes | 🇫🇷 Oui

🇺🇸 No | 🇫🇷 Non