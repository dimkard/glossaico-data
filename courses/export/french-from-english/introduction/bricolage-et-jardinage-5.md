# 🇫🇷 Bricolage et jardinage 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__In this lesson we will learn DIY and gardening vocabulary__
_Dans cette leçon, nous apprendrons le vocabulaire du jardinage et du bricolage_

## Vocabulary

🇺🇸 plants         | 🇫🇷 plantes  

🇺🇸 plants         | 🇫🇷 bricolage  

🇺🇸 garden         | 🇫🇷 jardin  

🇺🇸 gardening      | 🇫🇷 jardinage  

🇺🇸 hammer         | 🇫🇷 marteau  