# 🇫🇷 Apprendre des langues 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__In this lesson we will learn vocabulary to talk about languages__
_Dans cette leçon, nous apprendrons le vocabulaire pour parler de l'apprentissage des langues_.  

## Vocabulary

🇺🇸 french          | 🇫🇷 français   

🇺🇸 german          | 🇫🇷 allemand  

🇺🇸 english         | 🇫🇷 anglais  

🇺🇸 vocabulary list | 🇫🇷 vocabulary list  