# 👵 👴 👴 la famille élargie (the extended family) 👵 👴 👴

This course is all about family.
Ce cours concerne la famille.  

__Here is what you'll learn in this lesson:__
__Voici ce que vous apprendrez dans cette leçon:__

## Vocabulary

🇺🇸 uncle        |  🇫🇷 l’oncle 
🇺🇸 aunt         |  🇫🇷 la tante
🇺🇸 nephew       |  🇫🇷 le neveu
🇺🇸 niece        |  🇫🇷 la nièce 
🇺🇸 cousin (m)   |  🇫🇷 le cousin
🇺🇸 cousin (f)   |  🇫🇷 la cousine
🇺🇸 grand-parents|  🇫🇷 les grand-parents
🇺🇸 grandson     |  🇫🇷 le petit-fils
🇺🇸 ganddaugther |  🇫🇷 la petite-fille
🇺🇸 1st cousin (m)  🇫🇷 le cousin germain
🇺🇸 1st cousin (f)  🇫🇷 la cousin germaine