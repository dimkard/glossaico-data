# 🇫🇷 Faire la musique 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__In this lesson we will learn vocabulary to talk about making music.__
_Dans cette leçon, nous allons apprendre le vocabulaire dont vous avez besoin pour parler de faire de la musique._

## Vocabulary

🇺🇸 the guitar           | 🇫🇷 la guitare  

🇺🇸 the piano            | 🇫🇷 la piano

🇺🇸 the electric guitar  | 🇫🇷 la guitar électrique

🇺🇸 violin               | 🇫🇷 le geige

🇺🇸 drum                 | 🇫🇷 tambour

🇺🇸 triangle             | 🇫🇷 triangle

🇺🇸 trumpet              | 🇫🇷 trompette

🇺🇸 playing              | 🇫🇷 jouer