# 🍇🍈 Le Fruits 🍊🍉 ,

__Here is what you'll learn in this lesson:__
* Fruit names in french   

## Vocabulary

🇺🇸 Lingonberry   |  🇫🇷 Airelle
🇺🇸 Raspberry     |  🇫🇷 Framboise
🇺🇸 Star fruit    |  🇫🇷 Carambola
🇺🇸 Strawberry    |  🇫🇷 fraise
🇺🇸 melon         |  🇫🇷 melon
🇺🇸 Watermelon    |  🇫🇷 Pastèque
🇺🇸 mango         |  🇫🇷 la mangue
🇺🇸 olive         |  🇫🇷 l'olive (f)