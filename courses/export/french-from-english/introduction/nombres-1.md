# 🇫🇷 Numéros I - 1 - 10 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__We learn in this lesson to count from one to ten.__
_Nous apprenons dans cette leçon à compter de un à dix._

## Vocabulary

🇺🇸 One |  🇫🇷 Un  

🇺🇸 Two | 🇫🇷 Deux  

🇺🇸 Three | 🇫🇷 Trois  

🇺🇸 Four | 🇫🇷 Quatre  

🇺🇸 Five | 🇫🇷 Cinq  

🇺🇸 Six | 🇫🇷 Six  

🇺🇸 Seven | 🇫🇷 Sept  

🇺🇸 Eight | 🇫🇷 Huit  

🇺🇸 Nine | 🇫🇷 Neuf  

🇺🇸 Ten | 🇫🇷 Dix  