# 🇫🇷 Au cinema 🇫🇷,

__Nice that you'd like to learn some french.__
_C'est bien que vous souhaitiez apprendre le français._
__In this lesson we will learn vocabulary to talk about going to the movies__
_Dans cette leçon, nous allons apprendre du vocabulaire pour parler d'aller au cinéma_

## Vocabulary

🇺🇸 cinema         | 🇫🇷 cinéma  

🇺🇸 cheesy         | 🇫🇷 rigard

🇺🇸 comedies       | 🇫🇷 comédies

🇺🇸 meilleur       | 🇫🇷 best

🇺🇸 tickets        | 🇫🇷 tickets