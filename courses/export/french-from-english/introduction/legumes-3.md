# Le Légume,

__Here is what you'll learn in this lesson:__
_Vegetable names in french_

## Vocabulary

🇺🇸 garlic        |  🇫🇷 l’ail (m)
🇺🇸 asparagus     |  🇫🇷 l’asperge (f)
🇺🇸 carrot        |  🇫🇷 la carotte 
🇺🇸 cucumber      |  🇫🇷 le concombre
🇺🇸 onion         |  🇫🇷 l'oignon (m)'
🇺🇸 zucchini      |  🇫🇷 la courgette 
🇺🇸 shallot       |  🇫🇷 l’échalotte (f)
🇺🇸 spinach       |  🇫🇷 l’épinard (m)
🇺🇸 sweetcorn     |  🇫🇷 le maïs doux
🇺🇸 chilli pepper |  🇫🇷 le piment
🇺🇸 pumpkin       |  🇫🇷 le potimarron