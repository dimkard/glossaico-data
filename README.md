<!--
    SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
    SPDX-License-Identifier: CC-BY-SA-4.0
-->

# glossaico-data

LibreLingo course data needed by Glossaico

## Instructions

```
python3 -m venv env/
source env/bin/activate
python -m pip install requests
python -m pip install librelingo_yaml_loader
python -m pip install librelingo_json_export
cd scripts
python update_courses.py
```
